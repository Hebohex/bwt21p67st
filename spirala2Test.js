let chai = require('chai');
let chaiHttp= require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

let app = require('./spirala2');

describe('testiranje POST na /student', function () {
    before(function (done) {
        fs = require('fs');
        let novaLinija="Edib,Zahirović,67-ST,RS1"
        fs.writeFile('studenti.csv',novaLinija,function(err){
            if(err) throw err;
            });
        done();
    });
    it('Student sa indeksom vec postoji', function (done) {
        let student = {"ime": "Edib","prezime": "Zahirović","index": "67-ST","grupa": "RS1"};
        chai.request(app)
            .post('/student')
            .set('content-type', 'application/json')
            .send(student)
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.status.should.equal("Student sa indexom {67-ST} već postoji!");
                should.not.exist(err); 
                done();
            });
    });
    it('Kreiran novi student sa index-om {12345}', function (done) {
        let student = {"ime": "Neko","prezime": "Nekić","index": "12345","grupa": "RS1"};
        chai.request(app)
            .post('/student')
            .set('content-type', 'application/json')
            .send(student)
            .end(function (err, res) {
                res.should.have.status(200); 
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.status.should.equal("Kreiran student!");
                should.not.exist(err); 
                done();
            });
    });
    it('Novi student {12345} je već upisan', function (done) {
        let student = {"ime": "Neko","prezime": "Nekić","index": 12345,"grupa": "RS1"};
        chai.request(app)
            .post('/student')
            .set('content-type', 'application/json')
            .send(student)
            .end(function (err, res) {
                res.should.have.status(200); 
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.status.should.equal("Student sa indexom {12345} već postoji!");
                should.not.exist(err); 
                done();
            });
    });
});

describe('testiranje PUT na /student/:index', function () {
    /*before(function (done) {
        fs = require('fs');
        let novaLinija="Edib,Zahirović,67-ST,1"
        fs.writeFile('studenti.csv',novaLinija,function(err){
            if(err) throw err;
            });
        done();
    });*/
    it('Promijenjena grupa za studenta {67-ST} u RS3', function (done) {
        let grupa = {"grupa": "RS3"};
        chai.request(app)
            .put('/student/67-ST')
            .set('content-type', 'application/json')
            .send(grupa)
            .end(function (err, res) {
                res.should.have.status(200); 
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.status.should.equal("Promjenjena grupa studentu {67-ST}");
                should.not.exist(err); 
                done();
            });
    });
    it('Promijenjena grupa nepostojećem index-u', function (done) {
        let student = {"grupa": "RS2"};
        chai.request(app)
            .put('/student/54321')
            .set('content-type', 'application/json')
            .send(student)
            .end(function (err, res) {
                res.should.have.status(200); 
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.status.should.equal("Student sa indexom {54321} ne postoji");
                should.not.exist(err); 
                done();
            });
    });
    it('Promijenjena grupa za studenta {12345} u RS2', function (done) {
        let grupa = {"grupa": "RS2"};
        chai.request(app)
            .put('/student/12345')
            .set('content-type', 'application/json')
            .send(grupa)
            .end(function (err, res) {
                res.should.have.status(200); 
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.status.should.equal("Promjenjena grupa studentu {12345}");
                should.not.exist(err); 
                done();
            });
    });
});

describe('testiranje POST na /batch/student', function () {
    before(function (done) {
        fs = require('fs');
        let novaLinija="Edib,Zahirović,67-ST,RS1"
        fs.writeFile('studenti.csv',novaLinija,function(err){
            if(err) throw err;
            });
        done();
    });
    it('Jedan dodan, jedan postoji', function (done) {
        let student = "Edib,Zahirović,67-ST,RS1\r\nNeko,Nekić,12345,RS1";
        chai.request(app)
            .post('/batch/student')
            .set('content-type', 'text/csv')
            .send(student)
            .end(function (err, res) {
                res.should.have.status(200); 
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.status.should.equal("Dodano {1} studenata, a studenti {67-ST} već postoje!");
                should.not.exist(err); 
                done();
            });
    });
    it('Svi već postoje, dodano {0}', function (done) {
        let student = "Edib,Zahirović,67-ST,RS1\r\nNeko,Nekić,12345,RS1";
        chai.request(app)
            .post('/batch/student')
            .set('content-type', 'text/csv')
            .send(student)
            .end(function (err, res) {
                res.should.have.status(200); 
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.status.should.equal("Dodano {0} studenata, a studenti {67-ST,12345} već postoje!");
                should.not.exist(err); 
                done();
            });
    });
    it('Dodani novi {2}', function (done) {
        let student = "Drugi,Drugić,54321,RS1\r\nTreći,Trečić,67890,RS1";
        chai.request(app)
            .post('/batch/student')
            .set('content-type', 'text/csv')
            .send(student)
            .end(function (err, res) {
                res.should.have.status(200); 
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.status.should.equal("Dodano {2} studenata!");
                should.not.exist(err); 
                done();
            });
    });
});
