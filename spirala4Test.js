let chai = require('chai');
let chaiHttp= require('chai-http');
const { before } = require('mocha');
let should = chai.should();
const Sequelize = require('sequelize');
const sequelize = require('./baza.js');
const Student = require('./Student.js')(sequelize);
const Vjezba = require('./Vjezba.js')(sequelize);
const Grupa = require('./Grupa.js')(sequelize);

chai.use(chaiHttp);

let app = require('./spirala4.js');

describe('testiranje spirale4', function () {
    before(async () => {
        await Student.sync({ force: true })
        await Vjezba.sync({ force: true })
        await Grupa.sync({ force: true })
        await Grupa.create({
            naziv:"RS1"
        })
        await Student.create({
            ime:"Edib",
            prezime:"Zahirović",
            index:"67-ST",
            grupa:1
        })
      })
    describe('testiranje POST na /student', function(){
        it('Student sa indeksom vec postoji', function (done) {
            let student = {"ime": "Edib","prezime": "Zahirović","index": "67-ST","grupa": "RS1"};
            chai.request(app)
                .post('/student')
                .set('content-type', 'application/json')
                .send(student)
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Student sa indexom {67-ST} već postoji!");
                    should.not.exist(err); 
                    done();
                });
        });
        it('Kreiran novi student sa index-om {12345}', function (done) {
            let student = {"ime": "Neko","prezime": "Nekić","index": "12345","grupa": "RS2"};
            chai.request(app)
                .post('/student')
                .set('content-type', 'application/json')
                .send(student)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Kreiran student!");
                    should.not.exist(err); 
                    done();
                });
        });
        it('Novi student {12345} je već upisan', function (done) {
            let student = {"ime": "Neko","prezime": "Nekić","index": 12345,"grupa": "RS2"};
            chai.request(app)
                .post('/student')
                .set('content-type', 'application/json')
                .send(student)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Student sa indexom {12345} već postoji!");
                    should.not.exist(err); 
                    done();
                });
        });
    });
    describe('testiranje PUT na /student/:index', function () {
        it('Promijenjena grupa za studenta {67-ST} u RS2', function (done) {
            let grupa = {grupa: "RS2"};
            chai.request(app)
                .put('/student/67-ST')
                .set('content-type', 'application/json')
                .send(grupa)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Promjenjena grupa studentu {67-ST}");
                    should.not.exist(err); 
                    done();
                });
        });
        it('Promijenjena grupa nepostojećem index-u', function (done) {
            let student = {grupa: "RS1"};
            chai.request(app)
                .put('/student/54321')
                .set('content-type', 'application/json')
                .send(student)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Student sa indexom {54321} ne postoji");
                    should.not.exist(err); 
                    done();
                });
        });
        it('Promijenjena grupa za studenta {12345} u RS2', function (done) {
            let grupa = {"grupa": "RS1"};
            chai.request(app)
                .put('/student/12345')
                .set('content-type', 'application/json')
                .send(grupa)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Promjenjena grupa studentu {12345}");
                    should.not.exist(err); 
                    done();
                });
        });
    });
    describe('testiranje POST na /batch/student', function () {
        it('Svi već postoje, dodano {0}', function (done) {
            let student = "Edib,Zahirović,67-ST,RS2\r\nNeko,Nekić,12345,RS1";
            chai.request(app)
                .post('/batch/student')
                .set('content-type', 'text/csv')
                .send(student)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Dodano {0} studenata, a studenti {67-ST,12345} već postoje!");
                    should.not.exist(err); 
                    done();
                });
        });
        it('Jedan postoji, jedan dodan', function (done) {
            let student = "Edib,Zahirović,67-ST,RS2\r\nDrugi,Drugić,54321,RS1";
            chai.request(app)
                .post('/batch/student')
                .set('content-type', 'text/csv')
                .send(student)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Dodano {1} studenata, a studenti {67-ST} već postoje!");
                    should.not.exist(err); 
                    done();
                });
        });
        it('Dodani novi {1}', function (done) {
            let student = "Treći,Trečić,67890,RS3";
            chai.request(app)
                .post('/batch/student')
                .set('content-type', 'text/csv')
                .send(student)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Dodano {1} studenata!");
                    should.not.exist(err); 
                    done();
                });
        });
    })
    describe('testiranje POST na /vjezbe',function(){
        it('Kreiraj dvije vjezbe', function (done){
            let vjezbe={"brojVjezbi":2}
            chai.request(app)
                .post('/vjezbe')
                .set('content-type', 'application/json')
                .send(vjezbe)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Dodane 2 vjezbe");
                    should.not.exist(err); 
                    setTimeout(done, 500);
                });
        });
        it('Broj vjezbi -2', function (done){
            let vjezbe={"brojVjezbi":-2}
            chai.request(app)
                .post('/vjezbe')
                .set('content-type', 'application/json')
                .send(vjezbe)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Pogresan unos broja vjezbi!");
                    should.not.exist(err); 
                    done();
                });
        });
    });
    describe('testiranje POST na /student/:index/vjezba/:vjezba',function(){
        it('Ažurirana vježba, prvi put', function (done){
            let report={"testReport":"{\"stats\": {\"suites\": 2,\"tests\": 3,\"passes\": 2,\"pending\": 0,\"failures\": 1,\"start\": \"2021-11-05T15:00:26.343Z\",\"end\": \"2021-11-05T15:00:26.352Z\",\"duration\": 9},\"tests\": [{ \"title\": \"should draw 3 rows when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\", \"file\": null, \"duration\": 1, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} }, {\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} }, { \"title\": \"should draw 5 columns in row 1 when parameter are 3,3\", \"fullTitle\": \"Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} } ], \"pending\": [], \"failures\": [ { \"title\": \"should draw 2 columns in row 2 when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} } ], \"passes\": [ { \"title\": \"should draw 3 rows when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\", \"file\": null, \"duration\": 1, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} }, { \"title\": \"should draw 5 columns in row 1 when parameter are 3,3\", \"fullTitle\": \"Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {}}]}"}
            chai.request(app)
                .post('/student/67-ST/vjezba/1')
                .set('content-type', 'application/json')
                .send(report)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('vjezba');
                    res.body.should.have.property('tacnost');
                    res.body.should.have.property('promjena');
                    res.body.should.have.property('greske');
                    res.body.vjezba.should.equal(1);
                    res.body.tacnost.should.equal("66.7%");
                    res.body.promjena.should.equal("0%");
                    res.body.greske.should.deep.to.equal(["Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3 "]);
                    should.not.exist(err); 
                    done();
                });
        });
        it('Ažurirana vježba, drugi put', function (done){
            let report={"testReport":"{\"stats\": { \"suites\": 2, \"tests\": 3, \"passes\": 2, \"pending\": 0, \"failures\": 1, \"start\": \"2021-11-05T15:00:26.343Z\", \"end\": \"2021-11-05T15:00:26.352Z\", \"duration\": 9 }, \"tests\": [ { \"title\": \"should draw 3 rows when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\", \"file\": null, \"duration\": 1, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} }, {\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 1 columns in row 2 when parameter are 2,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} }, { \"title\": \"should draw 5 columns in row 1 when parameter are 3,3\", \"fullTitle\": \"Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} } ], \"pending\": [], \"failures\": [ { \"title\": \"should draw 2 columns in row 2 when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 1 columns in row 2 when parameter are 2,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} } ], \"passes\": [ { \"title\": \"should draw 3 rows when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\", \"file\": null, \"duration\": 1, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} }, { \"title\": \"should draw 5 columns in row 1 when parameter are 3,3\", \"fullTitle\": \"Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {}}]}"}
            chai.request(app)
                .post('/student/67-ST/vjezba/1')
                .set('content-type', 'application/json')
                .send(report)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('vjezba');
                    res.body.should.have.property('tacnost');
                    res.body.should.have.property('promjena');
                    res.body.should.have.property('greske');
                    res.body.vjezba.should.equal(1);
                    res.body.tacnost.should.equal("66.7%");
                    res.body.promjena.should.equal("50%");
                    res.body.greske.should.deep.to.equal(["Tabela crtaj() should draw 1 columns in row 2 when parameter are 2,3","Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3 "]);
                    should.not.exist(err); 
                    done();
                });
        })
        it('Nije moguće ažurirati vježbe, jer index ne postoji', function (done){
            let report={"testReport":"{\"stats\": {\"suites\": 2,\"tests\": 3,\"passes\": 2,\"pending\": 0,\"failures\": 1,\"start\": \"2021-11-05T15:00:26.343Z\",\"end\": \"2021-11-05T15:00:26.352Z\",\"duration\": 9},\"tests\": [{ \"title\": \"should draw 3 rows when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\", \"file\": null, \"duration\": 1, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} }, {\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} }, { \"title\": \"should draw 5 columns in row 1 when parameter are 3,3\", \"fullTitle\": \"Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} } ], \"pending\": [], \"failures\": [ { \"title\": \"should draw 2 columns in row 2 when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} } ], \"passes\": [ { \"title\": \"should draw 3 rows when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\", \"file\": null, \"duration\": 1, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} }, { \"title\": \"should draw 5 columns in row 1 when parameter are 3,3\", \"fullTitle\": \"Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {}}]}"}
            chai.request(app)
                .post('/student/66-ST/vjezba/1')
                .set('content-type', 'application/json')
                .send(report)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Nije moguće ažurirati vježbe!");
                    should.not.exist(err); 
                    done();
                });
        })
        it('Nije moguće ažurirati vježbe, jer vjezba ne postoji', function (done){
            let report={"testReport":"{\"stats\": {\"suites\": 2,\"tests\": 3,\"passes\": 2,\"pending\": 0,\"failures\": 1,\"start\": \"2021-11-05T15:00:26.343Z\",\"end\": \"2021-11-05T15:00:26.352Z\",\"duration\": 9},\"tests\": [{ \"title\": \"should draw 3 rows when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\", \"file\": null, \"duration\": 1, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} }, {\"title\": \"should draw 2 columns in row 2 when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} }, { \"title\": \"should draw 5 columns in row 1 when parameter are 3,3\", \"fullTitle\": \"Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} } ], \"pending\": [], \"failures\": [ { \"title\": \"should draw 2 columns in row 2 when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} } ], \"passes\": [ { \"title\": \"should draw 3 rows when parameter are 2,3\", \"fullTitle\": \"Tabela crtaj() should draw 3 rows when parameter are 2,3\", \"file\": null, \"duration\": 1, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {} }, { \"title\": \"should draw 5 columns in row 1 when parameter are 3,3\", \"fullTitle\": \"Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3\", \"file\": null, \"duration\": 0, \"currentRetry\": 0, \"speed\": \"fast\", \"err\": {}}]}"}
            chai.request(app)
                .post('/student/67-ST/vjezba/5')
                .set('content-type', 'application/json')
                .send(report)
                .end(function (err, res) {
                    res.should.have.status(200); 
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal("Nije moguće ažurirati vježbe!");
                    should.not.exist(err); 
                    done();
                });
        })
    })
});
