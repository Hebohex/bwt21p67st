exports.ts = class TestoviParser {
    dajTacnost(json) {
        let name=JSON.parse(json);
        this.testovi=name.stats.tests;
        this.tacni=name.stats.passes;

        if(this.testovi==0){
            return {"tacnost":"0%","greske":["Testovi se ne mogu izvršiti"]};
        }

        if(this.testovi==this.tacni){
            return {"tacnost":"100%","greske":[]};
        }

        var number = this.tacni/this.testovi*100
        var rounded = Math.round(number * 10) / 10

        let string=JSON.stringify(name.failures);
        const niz=string.split("},{");
        var greske="";
        for(let element of niz){
            greske=greske.concat(element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\""))," ");
        }

        return {"tacnost":rounded+"%","greske":[greske]};
        }

    porediRezultate(json1,json2){
        let rezultat1=JSON.parse(json1)
        let rezultat2=JSON.parse(json2)
        let testovi1=[];
        let testovi2=[];
        let niz=JSON.stringify(rezultat1.tests).split("},{");
        var i=0;
        for(let element of niz){
            testovi1[i]=element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\""));
            i++;
        }
        niz=JSON.stringify(rezultat2.tests).split("},{");
        i=0;
        for(let element of niz){
            testovi2[i]=element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\""));
            i++;
        }
        let neidenticni=0;
        for(let element1 of testovi1){
            i=0;
            for(let element2 of testovi2){
                if(element1==element2){
                    i=1;
                    break;
                }
            }
            if(i==0){
                neidenticni=1;
            }
        }

        let x;
        var greske="";
        if(neidenticni){
            let pali1=[];
            let pali2=[];
            let niz=JSON.stringify(rezultat1.failures).split("},{");
            var i=0;
            for(let element of niz){
                pali1[i]=element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\""));
                i++;
            }
            niz=JSON.stringify(rezultat2.failures).split("},{");
            i=0;
            for(let element of niz){
                pali2[i]=element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\""));
                i++;
            }

            let razliciti=0;
            for(let element1 of pali1){
                i=0;
                for(let element2 of testovi2){
                    if(element1==element2){
                        i=1;
                        break;
                    }
                }
                if(i==0 && element1!=""){
                    greske=greske.concat(element1);
                    razliciti++;
                }
            }
            for(let element2 of pali2){
                i=0;
                for(let element1 of pali1){
                    if(element1==element2){
                        i=1;
                        break;
                    }
                }
                if(i==0){
                    greske=greske.concat(element2);
                }
            }
            var number = (razliciti+rezultat2.stats.failures)/(razliciti+rezultat2.stats.tests)*100
            x = Math.round(number * 10) / 10 
            
        }else{
            var number = rezultat2.stats.passes/rezultat2.stats.tests*100
            x = Math.round(number * 10) / 10
            
            let niz=JSON.stringify(rezultat1.failures).split("},{");
            for(let element of niz){
                greske=greske.concat(element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\""))," ");
            }
        }

        return {"promjena":x+"%","greske":[greske]};
    }
  }