const Sequelize = require("sequelize");
const sequelize = require("./baza.js");
 
module.exports = function (sequelize, DataTypes) {
    const Vjezba = sequelize.define('Vjezba', {
       index: Sequelize.STRING,
       vjezba: Sequelize.INTEGER,
       tacnost: Sequelize.STRING,
       promjena: Sequelize.STRING,
       greske: Sequelize.JSON,
       testovi: Sequelize.JSON
   });
   return Vjezba;
}
