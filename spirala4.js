const Sequelize = require('sequelize');
const bodyParser = require('body-parser');
const sequelize = require('./baza.js');
const express = require("express");
const app = express();
var dt = require('./TestoviParser');
const Student = require('./Student.js')(sequelize);
const Vjezba = require('./Vjezba.js')(sequelize);
const Grupa = require('./Grupa.js')(sequelize);
Student.sync();
Grupa.sync();
Vjezba.sync();

app.use(express.static(__dirname));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

sequelize.authenticate()
    .then(() => console.log('Baza povezana...'))
    .catch(err => console.log('Error: ' + err))

    app.post('/student',(req,res)=>{
        let post = {
            ime: req.body['ime'],
            prezime: req.body['prezime'],
            index : req.body['index'],
            grupa : req.body['grupa']
        }
        
        let { ime,prezime,index,grupa } = post;
        let myJSON={status:''}

        Student.findOne({ where: { index: index } }).then(student=>{
        if (student == null) {
            Grupa.findOrCreate({where: { naziv: grupa }}).then(([gr,created]) => {
    
                var grid=gr.id;
                Student.create({
                    ime,
                    prezime,
                    index,
                    grupa:grid
                })
                .then(gig =>{
                    myJSON={status:"Kreiran student!"};
                    res.send(myJSON); 
                })
                .catch(err => console.log(err))
            });
        } else {
            myJSON={status:"Student sa indexom {"+index+"} već postoji!"};
            res.send(myJSON);
        }
        });
    });

    app.put('/student/:index',(req,res)=>{
        let post = {
            index : req.params.index,
            grupa : req.body['grupa']
        }
        
        let { index,grupa } = post;
        let myJSON={status:''}

        Student.findOne({ where: { index: index } }).then(student=>{
        if (student == null) {
            myJSON={status:"Student sa indexom {"+index+"} ne postoji"};
            res.send(myJSON);
        } else {
            Grupa.findOrCreate({where: { naziv: grupa }}).then(([gr,created]) => {
                var grid=gr.id;
                student.update({
                    grupa:grid
                })
                .then(gig =>{
                    myJSON={status:"Promjenjena grupa studentu {"+index+"}"};
                    res.send(myJSON); 
                })
                .catch(err => console.log(err))
            });
        }
        });
    });

    app.post('/batch/student',(req,res)=>{
        let tijeloZahtjeva = '';
        let noviStudenti=[];
        req.on('data',function(data){
            tijeloZahtjeva+=data;
            const noviStudent = tijeloZahtjeva.split("\r\n");
            i=0
            for(let elem of noviStudent){
                const podaci=elem.split(",")
                noviStudenti[i]= {"ime":podaci[0], "prezime":podaci[1], "index":podaci[2], "grupa":podaci[3]};
                i++;
            }
        
        let myJSON={status:"Dodano {"+noviStudenti.length+"} studenata!"};

        Student.findAll().then(studenti=>{
            let postoje=[]
            let m=0,n=0;
            let basNoviStudenti=[];
            for(let el1 of noviStudenti){
                let nepostoje=true
                for(let el2 of studenti){
                    if(el1.index==el2.index){
                        postoje[m]=el1.index;
                        m++
                        nepostoje=false
                    }
                }
                if(nepostoje){
                    basNoviStudenti[n]=el1;
                    n++
                }
            }
            if(m>0){
                myJSON={status:"Dodano {"+n+"} studenata, a studenti {"+postoje+"} već postoje!"}
            }
            for(let j=0;j<basNoviStudenti.length;j++){
                Grupa.findOrCreate({where: { naziv: basNoviStudenti[j].grupa }}).then(([gr,created]) => {
                    let a=basNoviStudenti[j].grupa;
                    if(isNaN(basNoviStudenti[j].grupa)){
                        for(let k=0;k<basNoviStudenti.length;k++){
                            if(basNoviStudenti[k].grupa==a){
                                basNoviStudenti[k].grupa=gr.id;
                            }
                        }
                    }
                    let elem = basNoviStudenti[j]
                    Student.create({
                        ime:elem.ime,
                        prezime:elem.prezime,
                        index:elem.index,
                        grupa:elem.grupa
                    })
                    .then(res.send(myJSON))
                    .catch(err => console.log(err));
                })
                .catch(err=>console.log(err))
            }
            if(n==0)
            res.send(myJSON);
        });
        });
    });

    app.post('/vjezbe',(req,res)=>{
        let post = {
            brVjezbi : req.body['brojVjezbi']
        }
        
        let { brVjezbi } = post;
        if(!Number.isInteger(brVjezbi) || brVjezbi<=0){  
        let myJSON={status:"Pogresan unos broja vjezbi!"};
        res.send(myJSON);  
        }else{
        Vjezba.truncate({ cascade: true }).then(()=>{
            Student.findAll().then(studenti=>{
            for(let elem of studenti){
                for(i=0;i<brVjezbi;i++){
                    Vjezba.create({
                        index: elem.index,
                        vjezba: (i+1),
                        tacnost: "0%",
                        promjena: "0%",
                        greske: [],
                        testovi: []
                    })
                    .catch(err => console.log(err))
                } 
            }
        });
        });
        let myJSON={status:"Dodane "+brVjezbi+" vjezbe"};
        res.send(myJSON);
    }
    });

    app.post('/student/:index/vjezba/:vjezba',(req,res)=>{
        let post = {
            index : req.params.index,
            vjezba: req.params.vjezba,
            report: req.body['testReport']
        }
        
        let { index,vjezba,report } = post;
        let myJSON={status:''}
        Vjezba.findOne({ where: {[Sequelize.Op.and]:[{index: index},{vjezba:vjezba}]}}).then(elem=>{
            if(elem==null){
                myJSON={status:"Nije moguće ažurirati vježbe!"};
                res.json(myJSON);
            }else{
                let para=JSON.parse(report);
                if(JSON.stringify(elem.testovi)=="[]"){
                    let k1=new dt.ts();
                    let test=k1.dajTacnost(report);
                    elem.tacnost=test.tacnost;
                    elem.greske=test.greske;
                    var gre=[];
                    let j=0
                    if(para.stats.passes!=0){
                        let string=JSON.stringify(para.passes);
                        let niz=string.split("},{");
                        for(let element of niz){
                            gre[j]={"fullTitle":element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\"")),"status":"pass"};
                            j++
                        }
                    }
                    if(para.stats.failures!=0){
                        string=JSON.stringify(para.failures);
                        niz=string.split("},{");
                        for(let element of niz){
                            gre[j]={"fullTitle":element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\"")),"status":"fail"};
                            j++
                        }
                    }
                    elem.testovi=gre;
                }else{
                    let rezultat1=JSON.parse(report);
                    let rezultat2=elem.testovi;
                    let testovi1=[];
                    let testovi2=[];
                    let niz=JSON.stringify(rezultat1.tests).split("},{");
                    i=0;
                    for(let element of niz){
                        testovi1[i]=element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\""));
                        i++;
                    }
                    i=0;
                    var pass=0,fail=0,brTestova=0
                    for(let el of rezultat2){
                        testovi2[i]=el.fullTitle;
                        i++;
                        brTestova++
                        if(el.status=="pass")pass++
                        if(el.status=="fail")fail++
                    }
                    let neidenticni=0;
                    for(let element1 of testovi1){
                        i=0;
                        for(let element2 of testovi2){
                            if(element1==element2){
                                i=1;
                                break;
                            }
                        }
                        if(i==0){
                            neidenticni=1;
                        }
                    }
                    let x;
                    var greske=[];
                    var k=0;
                    if(neidenticni){
                        let pali1=[];
                        let pali2=[];
                        let niz=JSON.stringify(rezultat1.failures).split("},{");
                        i=0;
                        for(let element of niz){
                            pali1[i]=element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\""));
                            i++;
                        }
                        i=0;
                        niz=JSON.stringify(elem.greske).split("\",\"" && "\"]");
                        for(let element of niz){
                            if(element!=""){
                            pali2[i]=element.substring(element.indexOf("Tabela crtaj()"));
                            i++;
                            }
                        }
                        let razliciti=0;
                        for(let element1 of pali1){
                            i=0;
                            for(let element2 of testovi2){
                                if(element1==element2){
                                    i=1;
                                    break;
                                }
                            }
                            if(i==0 && element1!=""){
                                greske[k]=element1;
                                k++
                                razliciti++;
                            }
                        }
                        for(let element2 of pali2){
                            i=0;
                            for(let element1 of pali1){
                                if(element1==element2){
                                    i=1;
                                    break;
                                }
                            }
                            if(i==0){
                                greske[k]=element2;
                                k++
                            }
                        }
                        var number = (razliciti+fail)/(razliciti+brTestova)*100
                        x = Math.round(number * 10) / 10    
                    }else{
                        var number = pass/brTestova*100
                        x = Math.round(number * 10) / 10
                        
                        let niz=JSON.stringify(rezultat1.failures).split("},{");
                        for(let element of niz){
                            greske[k]=element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\""));
                            k++
                        }
                    }
                    let k1=new dt.ts();
                    let test=k1.dajTacnost(report)
                    elem.promjena=x+"%";
                    elem.tacnost=test.tacnost;
                    elem.greske=greske;
                    var gre=[];
                    let j=0
                    if(para.stats.passes!=0){
                        let string=JSON.stringify(para.passes);
                        let niz=string.split("},{");
                        for(let element of niz){
                            gre[j]={"fullTitle":element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\"")),"status":"pass"};
                            j++
                        }
                    }
                    if(para.stats.failures!=0){
                        string=JSON.stringify(para.failures);
                        niz=string.split("},{");
                        for(let element of niz){
                            gre[j]={"fullTitle":element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\"")),"status":"fail"};
                            j++
                        }
                    }
                    elem.testovi=gre;
                }
                myJSON={"vjezba":elem.vjezba,"tacnost":elem.tacnost,"promjena":elem.promjena,"greske":elem.greske};
                elem.update({
                    tacnost:elem.tacnost,
                    promjena:elem.promjena,
                    greske:elem.greske,
                    testovi:elem.testovi
                })
                .then(res.send(myJSON))
                .catch(err=>console.log(err))
            }
        });
    });

app.listen(3000, function() {
    console.log('Connected to port 3000');
});

module.exports=app;