let XMLHttpRequest=require('xhr2');
class Spirala3{
    posaljiStudent(studentObjekat,callback){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200){ 
                var odg = JSON.parse(ajax.responseText);
                if(odg.status=="Kreiran student!"){
                    callback(null, JSON.parse(ajax.responseText));
                }else{
                    callback(JSON.parse(ajax.responseText),null);
                }
            }else{
                callback(ajax.status,null);
            }
        }
        ajax.open("POST", "http://localhost:3000/student", true);
        ajax.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');
        ajax.send(studentObjekat);
    }

    postaviGrupu(indexStudenta,grupa,callback){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState == 4){ 
                var odg = JSON.parse(ajax.responseText);
                if(odg.status=="Promjenjena grupa studentu {"+indexStudenta+"}"){
                    callback(null, JSON.parse(ajax.responseText));
                }else{
                    callback(JSON.parse(ajax.responseText),null);
                }
            }else{
                callback(ajax.status,null);
            }
        }
        ajax.open("PUT", "http://localhost:3000/student/"+indexStudenta, true);
        ajax.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');
        ajax.send(grupa);
    }

    posaljiStudente(studentiCSVString,callback){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {// Anonimna funkcija
            const noviStudent = studentiCSVString.split("\r\n");
            var i=0
            for(let elem of noviStudent){
                i++
            }
            if (ajax.readyState == 4 && ajax.status == 200){ 
                var odg = JSON.parse(ajax.responseText);
                if(odg.status=="Dodano {"+i+"} studenata!"){
                    callback(null, JSON.parse(ajax.responseText));
                }else{
                    callback(JSON.parse(ajax.responseText),null);
                }
            }else{
                callback(ajax.status,null);
            }
        }
        ajax.open("POST", "http://localhost:3000/batch/student", true);
        ajax.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');
        ajax.send(studentiCSVString);
    }

    postaviVjezbe(brojVjezbi,callback){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200){ 
                callback(null, {status:"Vjezbe unesene"});
            }else{
                callback({status:"Vjezbe nisu unesene"},null);
            }
        }
        ajax.open("POST", "http://localhost:3000/vjezbe", true);
        ajax.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');
        if(!Number.isInteger(brojVjezbi) || brojVjezbi<0)
            callback(404);
        else{
        var vjezbe = {"brojVjezbi": brojVjezbi};
        ajax.send(JSON.stringify(vjezbe));
        }
    }

    postaviTestReport(indexStudenta,nazivVjezbe,testReport,callback){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200){ 
                var odg = JSON.parse(ajax.responseText);
                if(odg.status=="Nije moguće ažurirati vježbe!"){
                    callback(JSON.parse(ajax.responseText),null);
                }else{
                    callback(null, JSON.parse(ajax.responseText));
                }
            }else{
                callback(ajax.status,null);
            }
        }
        ajax.open("POST", "http://localhost:3000/student/"+indexStudenta+"/vjezba/"+nazivVjezbe, true);
        ajax.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');
        ajax.send(testReport);
    }
};
module.exports=Spirala3;