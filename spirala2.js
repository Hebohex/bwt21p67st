const express= require('express');
const url= require('url');
const fs=require('fs');
const app=express();
var dt = require('./TestoviParser');

//prvi
app.post('/student',(req,res)=>{
    let tijeloZahtjeva = '';
       req.on('data',function(data){
           tijeloZahtjeva+=data;
       });
       let studenti=[]
       fs.readFile("studenti.csv", "utf8", function(err, data){
        if(err) throw err;
        const student = data.split("\r\n");
        let i=0
        for(let elem of student){
            const podaci=elem.split(",")
            studenti[i]= {"ime":podaci[0], "prezime":podaci[1], "index":podaci[2], "grupa":podaci[3]};
            i++
        }
        let myJSON={status:''}
        let param=JSON.parse(tijeloZahtjeva)
        let postoji=false
        for(let elem of studenti){
           if(elem.index==param.index){
                myJSON={status:"Student sa indexom {"+param.index+"} već postoji!"}
                postoji=true
                break;
           }
        }
           if(!postoji){
            myJSON={status:"Kreiran student!"}
            let novaLinija = "\r\n"+param.ime+","+param.prezime+","+param.index+","+param.grupa;
            fs.appendFile('studenti.csv',novaLinija,function(err){
             if(err) throw err;
             console.log("Novi red uspješno dodan!");
         });    
        }
        res.send(myJSON);
       });
});
//drugi
app.put('/student/:index',(req,res)=>{
    let tijeloZahtjeva = '';
        req.on('data',function(data){
            tijeloZahtjeva+=data;
        });
        let studenti=[]
        fs.readFile("studenti.csv", "utf8", function(err, data){
         if(err) throw err;
         const student = data.split("\r\n");
         let i=0
         for(let elem of student){
             const podaci=elem.split(",")
             studenti[i]= {"ime":podaci[0], "prezime":podaci[1], "index":podaci[2], "grupa":podaci[3]};
             i++
         }
         let myJSON={status:''}
         let param=JSON.parse(tijeloZahtjeva)
         let postoji=false
         for(let elem of studenti){
            if(elem.index==req.params.index){
                elem.grupa=param.grupa;
                 postoji=true
                 break;
            }
         }
            if(!postoji){
             myJSON={status:"Student sa indexom {"+req.params.index+"} ne postoji"}
             }else{
                myJSON={status:"Promjenjena grupa studentu {"+req.params.index+"}"};
                let novaLinija=""
                for(let el of studenti){
                    if(novaLinija!="")
                    novaLinija=novaLinija.concat("\r\n")
                    novaLinija=novaLinija.concat(el.ime+","+el.prezime+","+el.index+","+el.grupa)
                }
                fs.writeFile('studenti.csv',novaLinija,function(err){
                 if(err) throw err;
                 });
            }
            res.send(myJSON);      
        });
});

//treci
app.post('/batch/student',(req,res)=>{
    let tijeloZahtjeva = '';
       req.on('data',function(data){
           tijeloZahtjeva+=data;
       });
       let studenti=[]
       fs.readFile("studenti.csv", "utf8", function(err, data){
        if(err) throw err;
        const student = data.split("\r\n");
        let i=0
        for(let elem of student){
            const podaci=elem.split(",")
            studenti[i]= {"ime":podaci[0], "prezime":podaci[1], "index":podaci[2], "grupa":podaci[3]};
            i++
        }
        let noviStudenti=[];
        const noviStudent = tijeloZahtjeva.split("\r\n");
        i=0
        for(let elem of noviStudent){
            const podaci=elem.split(",")
            noviStudenti[i]= {"ime":podaci[0], "prezime":podaci[1], "index":podaci[2], "grupa":podaci[3]};
            i++
        }
        let myJSON={status:"Dodano {"+noviStudenti.length+"} studenata!"}
        let postoje=[]
        let m=0,n=0;
        let basNoviStudenti=[];
        for(let el1 of noviStudenti){
            let nepostoje=true
            for(let el2 of studenti){
                if(el1.index==el2.index){
                    postoje[m]=el1.index;
                    m++
                    nepostoje=false
                }
            }
            if(nepostoje){
                basNoviStudenti[n]=el1;
                n++
            }
        }
        if(m>0){
            myJSON={status:"Dodano {"+n+"} studenata, a studenti {"+postoje+"} već postoje!"}
        }
        let novaLinija=""
        for(let elem of basNoviStudenti){
            novaLinija = novaLinija.concat ("\r\n"+elem.ime+","+elem.prezime+","+elem.index+","+elem.grupa);
        }
        fs.appendFile('studenti.csv',novaLinija,function(err){
             if(err) throw err;
             if(novaLinija!="")
             console.log("Novi red uspješno dodan!");
        });
        //res.writeHead(200,{});
        res.send(myJSON);
        //res.end(JSON.stringify(myJSON));
       });
});

//cetvrti
app.post('/vjezbe',(req,res)=>{
    let tijeloZahtjeva = '';
    req.on('data',function(data){
        tijeloZahtjeva+=data;
    });
    let studenti=[]
    fs.readFile("studenti.csv", "utf8", function(err, data){
     if(err) throw err;
     const student = data.split("\r\n");
     let i=0
     for(let elem of student){
         const podaci=elem.split(",")
         studenti[i]= {"ime":podaci[0], "prezime":podaci[1], "index":podaci[2], "grupa":podaci[3]};
         i++
     }
     let param=JSON.parse(tijeloZahtjeva)
     let novaLinija=""
     for(let elem of studenti){
         for(i=0;i<param.brojVjezbi;i++){
             if(novaLinija!=""){
             novaLinija =novaLinija.concat("\r\n")
            }
            novaLinija = novaLinija.concat(elem.index+" , "+(i+1)+" , 0% , 0% , [] , []");
         } 
     }
     fs.writeFile('vjezbe.csv',novaLinija,function(err){
        if(err) throw err;
       });
     res.writeHead(200,{});
     res.end();
    });
});

//peti
app.post('/student/:index/vjezba/:vjezba',(req,res)=>{
    let tijeloZahtjeva = '';
    req.on('data',function(data){
        tijeloZahtjeva+=data;
    });
    let studenti=[]
    fs.readFile("vjezbe.csv", "utf8", function(err, data){
     if(err) throw err;
     const student = data.split("\r\n");
     let i=0
     for(let elem of student){
         const podaci=elem.split(" , ")
         studenti[i]= {"index":podaci[0], "vjezba":podaci[1], "tacnost":podaci[2], "promjena":podaci[3], "greska":podaci[4], "testovi":podaci[5]};
         i++
     }
     let para=JSON.parse(tijeloZahtjeva);
     let myJSON={"status":"Nije moguće ažurirati vježbe!"}
     for(let elem of studenti){
        if(elem.index==req.params.index && elem.vjezba==req.params.vjezba){
            if(elem.testovi=="[]"){
            let k1=new dt.ts();
            let test=k1.dajTacnost(tijeloZahtjeva)
            elem.tacnost=test.tacnost;
            elem.greska=JSON.stringify(test.greske);
            var gre=[];
            let j=0
            if(para.stats.passes!=0){
                let string=JSON.stringify(para.passes);
                let niz=string.split("},{");
                for(let element of niz){
                    gre[j]={"fullTitle":element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\"")),"status":"pass"};
                    j++
                }
            }
            if(para.stats.failures!=0){
                string=JSON.stringify(para.failures);
                niz=string.split("},{");
                for(let element of niz){
                    gre[j]={"fullTitle":element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\"")),"status":"fail"};
                    j++
                }
            }
            elem.testovi=JSON.stringify(gre);
            
        }else{
                let rezultat1=JSON.parse(tijeloZahtjeva)
                let rezultat2=JSON.parse(elem.testovi)
                let testovi1=[];
                let testovi2=[];
                let niz=JSON.stringify(rezultat1.tests).split("},{");
                i=0;
            for(let element of niz){
                testovi1[i]=element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\""));
                i++;
            }
            i=0;
            var pass=0,fail=0,brTestova=0
            for(let el of rezultat2){
                testovi2[i]=el.fullTitle;
                i++;
                brTestova++
                if(el.status=="pass")pass++
                if(el.status=="fail")fail++
            }
            let neidenticni=0;
            for(let element1 of testovi1){
                i=0;
                for(let element2 of testovi2){
                    if(element1==element2){
                        i=1;
                        break;
                    }
                }
                if(i==0){
                    neidenticni=1;
                }
            }
            let x;
            var greske=[];
            var k=0;
            if(neidenticni){
                let pali1=[];
                let pali2=[];
                let niz=JSON.stringify(rezultat1.failures).split("},{");
                i=0;
                for(let element of niz){
                    pali1[i]=element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\""));
                    i++;
                }
                i=0;
                niz=elem.greska.split("\",\"" && "\"]");
                for(let element of niz){
                    if(element!=""){
                    pali2[i]=element.substring(element.indexOf("Tabela crtaj()"));
                    i++;
                }
                }
                let razliciti=0;
                for(let element1 of pali1){
                    i=0;
                    for(let element2 of testovi2){
                        if(element1==element2){
                            i=1;
                            break;
                        }
                    }
                    if(i==0 && element1!=""){
                        greske[k]=element1;
                        k++
                        razliciti++;
                    }
                }
                for(let element2 of pali2){
                    i=0;
                    for(let element1 of pali1){
                        if(element1==element2){
                            i=1;
                            break;
                        }
                    }
                    if(i==0){
                        greske[k]=element2;
                        k++
                    }
                }
                var number = (razliciti+fail)/(razliciti+brTestova)*100
                x = Math.round(number * 10) / 10 
                
            }else{
                var number = pass/brTestova*100
                x = Math.round(number * 10) / 10
                
                let niz=JSON.stringify(rezultat1.failures).split("},{");
                for(let element of niz){
                    greske[k]=element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\""));
                    k++
                }
            }
            let k1=new dt.ts();
            let test=k1.dajTacnost(tijeloZahtjeva)
            elem.promjena=x+"%";
            elem.tacnost=test.tacnost;
            elem.greska=JSON.stringify(greske);
            var gre=[];
            let j=0
            if(para.stats.passes!=0){
                let string=JSON.stringify(para.passes);
                let niz=string.split("},{");
                for(let element of niz){
                    gre[j]={"fullTitle":element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\"")),"status":"pass"};
                    j++
                }
            }
            if(para.stats.failures!=0){
                string=JSON.stringify(para.failures);
                niz=string.split("},{");
                for(let element of niz){
                    gre[j]={"fullTitle":element.substring(element.indexOf("Tabela crtaj()"),element.indexOf("\",\"file\"")),"status":"fail"};
                    j++
                }
            }
            elem.testovi=JSON.stringify(gre);
            
        }
            myJSON={"vjezba":elem.vjezba,"tacnost":elem.tacnost,"promjena":elem.promjena,"greske":elem.greska};
        }
     }
     let novaLinija=""
     for(let elem of studenti){
        if(novaLinija!=""){
            novaLinija =novaLinija.concat("\r\n")
           }
        novaLinija = novaLinija.concat(elem.index+" , "+elem.vjezba+" , "+elem.tacnost+" , "+elem.promjena+" , "+elem.greska+" , "+elem.testovi);
    }
    fs.writeFile('vjezbe.csv',novaLinija,function(err){
       if(err) throw err;
      });
     res.send(myJSON);
    });
});

app.listen(3000, ()=>console.log("Port 3000"));

module.exports=app;