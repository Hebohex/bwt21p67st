let chai=require('chai');
const express = require('express');
let Spirala3=require('./AjaxPozivi.js')
chai.should();
let app = require('./spirala2');
app.use(express.static(__dirname));

//prva ruta 
describe('Testovi posaljiStudent', function() {
    before(function (done) {
        fs = require('fs');
        let novaLinija="Edib,Zahirović,67-ST,RS1"
        fs.writeFile('studenti.csv',novaLinija,function(err){
            if(err) throw err;
            });
        done();
    });
  it('Kreiraj novog studenta', function(done) {
    let student = {"ime": "Edib","prezime": "Zahirović","index": "69-ST","grupa": "RS1"};
    var data = { status: 'Kreiran student!' };
    let k1=new Spirala3();
    k1.posaljiStudent(JSON.stringify(student),function(err, result) {
        if(result!=null){
            result.should.deep.equal(data);
            done();
        }
    });
  });

  it('Prethodno kreirani student postoji', function(done) {
    let student = {"ime": "Edib","prezime": "Zahirović","index": "69-ST","grupa": "RS1"};
    var data = { status: "Student sa indexom {69-ST} već postoji!" };
    let k1=new Spirala3();
    k1.posaljiStudent(JSON.stringify(student),function(err, result) {
        if(err!=null && !Number.isInteger(err)){
            err.should.deep.equal(data);
            done();
        }
    });
  });
});
//druga ruta
describe('Testovi postaviGrupu', function() {
    it('Promijeni grupu postojećem', function(done) {
      var data = { status: 'Promjenjena grupa studentu {67-ST}' };
      let grupa = {"grupa": "RS3"};
      let index="67-ST";
      let k1=new Spirala3();
      k1.postaviGrupu(index,JSON.stringify(grupa),function(err, result) {
          if(result!=null){
              result.should.deep.equal(data);
              done();
          }
      });
    });
  
    it('Greška pri promjeni grupe nepostojećem', function(done) {
        var data = { status: "Student sa indexom {22-ST} ne postoji" };
        let grupa = {"grupa": "RS3"};
        let index="22-ST";
        let k1=new Spirala3();
        k1.postaviGrupu(index,JSON.stringify(grupa),function(err, result) {
            if(err!=null && !Number.isInteger(err)){
                err.should.deep.equal(data);
                done();
            }
        });
    });
  });
  //treca ruta
  describe('Testovi posaljiStudente', function() {

    it('Dodaj nova 2 studenta', function(done) {
      let student = 'Drugi,Drugić,54321,RS1\r\nTreći,Trečić,67890,RS1';
      var data = { status: "Dodano {2} studenata!" };
      let k1=new Spirala3();
      k1.posaljiStudente(student,function(err, result) {
          if(result!=null){
            result.should.deep.equal(data);
            done();
          }
      });
    });
  
    it('Jedan student postoji, drugi je dodan', function(done) {
        let student = 'Edib,Zahirović,67-ST,RS1\r\nNeko,Nekić,12345,RS1';
        var data = { status: "Dodano {1} studenata, a studenti {67-ST} već postoje!" };
        let k1=new Spirala3();
        k1.posaljiStudente(student,function(err, result) {
            if(err!=null && !Number.isInteger(err)){
            err.should.deep.equal(data);
            done();
          }
      });
    });

    it('Svi već postoje', function(done) {
        let student = 'Edib,Zahirović,67-ST,RS1\r\nNeko,Nekić,12345,RS1';
        var data = { status: "Dodano {0} studenata, a studenti {67-ST,12345} već postoje!" };
        let k1=new Spirala3();
        k1.posaljiStudente(student,function(err, result) {
            if(err!=null && !Number.isInteger(err)){
            err.should.deep.equal(data);
            done();
          }
      });
    });
  });
  //cetvrta ruta
  describe('Testovi postaviVjezbe', function() {
      it('Unesi 2 vjezbe', function(done) {
        var data = {status:"Vjezbe unesene"};
        let k1=new Spirala3();
        k1.postaviVjezbe(2,function(err, result) {
            if(result!=null){
                result.should.deep.equal(data);
                done();
            }
        });
      });
    
      it('Greška pri postavljanu negativnog broja vježbi', function(done) {
          var data = {status:"Vjezbe nisu unesene"};
          let k1=new Spirala3();
          k1.postaviVjezbe(-2,function(err, result) {
              if(err!=null && !Number.isInteger(err)){
                  err.should.deep.equal(data);
                  done();
              }
          });
      });

      it('Greška pri postavljanu decimalnog broja vježbi', function(done) {
        var data = {status:"Vjezbe nisu unesene"};
        let k1=new Spirala3();
        k1.postaviVjezbe(2.5,function(err, result) {
            if(err!=null && !Number.isInteger(err)){
                err.should.deep.equal(data);
                done();
            }
        });
    });
    });
    //peta ruta
    describe('Testovi postaviTestReport', function() {

        it('Postoji indeks i vjezba', function(done) {
            var data = { vjezba: "1", tacnost: "66.7%", promjena: "0%", greske: "[\"Tabela crtaj() should draw 1 columns in row 2 when parameter are 2,3 \"]" };
            let index='67-ST';
            let vjezba='1';
            let testReport = '{"stats": { "suites": 2, "tests": 3, "passes": 2, "pending": 0, "failures": 1, "start": "2021-11-05T15:00:26.343Z", "end": "2021-11-05T15:00:26.352Z", "duration": 9 }, "tests": [ { "title": "should draw 3 rows when parameter are 2,3", "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3", "file": null, "duration": 1, "currentRetry": 0, "speed": "fast", "err": {} }, {"title": "should draw 2 columns in row 2 when parameter are 2,3", "fullTitle": "Tabela crtaj() should draw 1 columns in row 2 when parameter are 2,3", "file": null, "duration": 0, "currentRetry": 0, "speed": "fast", "err": {} }, { "title": "should draw 5 columns in row 1 when parameter are 3,3", "fullTitle": "Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3", "file": null, "duration": 0, "currentRetry": 0, "speed": "fast", "err": {} } ], "pending": [], "failures": [ { "title": "should draw 2 columns in row 2 when parameter are 2,3", "fullTitle": "Tabela crtaj() should draw 1 columns in row 2 when parameter are 2,3", "file": null, "duration": 0, "currentRetry": 0, "speed": "fast", "err": {} } ], "passes": [ { "title": "should draw 3 rows when parameter are 2,3", "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3", "file": null, "duration": 1, "currentRetry": 0, "speed": "fast", "err": {} }, { "title": "should draw 5 columns in row 1 when parameter are 3,3", "fullTitle": "Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3", "file": null, "duration": 0, "currentRetry": 0, "speed": "fast", "err": {}}]}';
            let k1=new Spirala3();
            k1.postaviTestReport(index,vjezba,testReport,function(err, result) {
              if(result!=null){
                result.should.deep.equal(data);
                done();
              }
          });
        });
      
        it('Pogrešan index', function(done) {
            var data={ status: "Nije moguće ažurirati vježbe!" };
            let index='22-ST';
            let vjezba='1';
            let testReport = '{"stats": { "suites": 2, "tests": 3, "passes": 2, "pending": 0, "failures": 1, "start": "2021-11-05T15:00:26.343Z", "end": "2021-11-05T15:00:26.352Z", "duration": 9 }, "tests": [ { "title": "should draw 3 rows when parameter are 2,3", "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3", "file": null, "duration": 1, "currentRetry": 0, "speed": "fast", "err": {} }, {"title": "should draw 2 columns in row 2 when parameter are 2,3", "fullTitle": "Tabela crtaj() should draw 1 columns in row 2 when parameter are 2,3", "file": null, "duration": 0, "currentRetry": 0, "speed": "fast", "err": {} }, { "title": "should draw 5 columns in row 1 when parameter are 3,3", "fullTitle": "Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3", "file": null, "duration": 0, "currentRetry": 0, "speed": "fast", "err": {} } ], "pending": [], "failures": [ { "title": "should draw 2 columns in row 2 when parameter are 2,3", "fullTitle": "Tabela crtaj() should draw 1 columns in row 2 when parameter are 2,3", "file": null, "duration": 0, "currentRetry": 0, "speed": "fast", "err": {} } ], "passes": [ { "title": "should draw 3 rows when parameter are 2,3", "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3", "file": null, "duration": 1, "currentRetry": 0, "speed": "fast", "err": {} }, { "title": "should draw 5 columns in row 1 when parameter are 3,3", "fullTitle": "Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3", "file": null, "duration": 0, "currentRetry": 0, "speed": "fast", "err": {}}]}';
            let k1=new Spirala3();
            k1.postaviTestReport(index,vjezba,testReport,function(err, result) {
               if(err!=null && !Number.isInteger(err)){
                err.should.deep.equal(data);
                done();
              }
          });
        });

        it('Pogrešna vježba', function(done) {
            var data={ status: "Nije moguće ažurirati vježbe!" };
            let index='67-ST';
            let vjezba='4';
            let testReport = '{"stats": { "suites": 2, "tests": 3, "passes": 2, "pending": 0, "failures": 1, "start": "2021-11-05T15:00:26.343Z", "end": "2021-11-05T15:00:26.352Z", "duration": 9 }, "tests": [ { "title": "should draw 3 rows when parameter are 2,3", "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3", "file": null, "duration": 1, "currentRetry": 0, "speed": "fast", "err": {} }, {"title": "should draw 2 columns in row 2 when parameter are 2,3", "fullTitle": "Tabela crtaj() should draw 1 columns in row 2 when parameter are 2,3", "file": null, "duration": 0, "currentRetry": 0, "speed": "fast", "err": {} }, { "title": "should draw 5 columns in row 1 when parameter are 3,3", "fullTitle": "Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3", "file": null, "duration": 0, "currentRetry": 0, "speed": "fast", "err": {} } ], "pending": [], "failures": [ { "title": "should draw 2 columns in row 2 when parameter are 2,3", "fullTitle": "Tabela crtaj() should draw 1 columns in row 2 when parameter are 2,3", "file": null, "duration": 0, "currentRetry": 0, "speed": "fast", "err": {} } ], "passes": [ { "title": "should draw 3 rows when parameter are 2,3", "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3", "file": null, "duration": 1, "currentRetry": 0, "speed": "fast", "err": {} }, { "title": "should draw 5 columns in row 1 when parameter are 3,3", "fullTitle": "Tabela crtaj() should draw 5 columns in row 1 when parameter are 3,3", "file": null, "duration": 0, "currentRetry": 0, "speed": "fast", "err": {}}]}';
            let k1=new Spirala3();
            k1.postaviTestReport(index,vjezba,testReport,function(err, result) {
               if(err!=null && !Number.isInteger(err)){
                err.should.deep.equal(data);
                done();
              }
          });
        });
      });